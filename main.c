#include <stdio.h>
#include "instancias.h"
#include "funciones.h"

int main(){

    int oper = 0;
    Persona p;

    p = crearPersona(p);

    while(oper != 3) {
        printf("Ingrese el numero de la operacion a realizar \n");
        printf("1)Cambiar datos de la persona \n");
        printf("2)Mostrar datos de la persona \n");
        printf("3)Salir \n");
        scanf("%d", &oper);
        switch (oper) {
            case 1:
                p = cambiarDatosPersona(p);
                break;
            case 2:
                mostrarDatosPersona(p);
                break;
            case 3:
                printf("Adios \n");
                break;
            default:
                printf("Operacion invalida, vuelve a intentar \n");
        }
    }

    return 0;
}