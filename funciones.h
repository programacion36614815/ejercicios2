#include <stdio.h>
#include <string.h>
#include "instancias.h"


Persona ingresarNombre(Persona p){

    printf("Ingresar nombre: \n");
    scanf("%s", p.nombre);

    return p;
}


Persona ingresarApellido(Persona p){

    printf("Ingresar apellido: \n");
    scanf("%s", p.apellido);

    return p;
}

Persona ingresarEdad(Persona p){

    printf("Ingresar edad: \n");
    scanf("%d", &p.edad);

    return p;
}

Persona crearPersona(Persona p){

    printf("Ingresar datos de la persona \n");
    p = ingresarNombre(p);
    p = ingresarApellido(p);
    p = ingresarEdad(p);

    return p;
}

Persona cambiarDatosPersona(Persona p){

    int oper = 0;

    while(oper != 4) {
        printf("Ingrese el numero del dato que quiere cambiar \n");
        printf("1)Nombre \n");
        printf("2)Apellido \n");
        printf("3)Edad \n");
        printf("4)Volver \n");
        scanf("%d", &oper);
        switch (oper) {
            case 1:
                p = ingresarNombre(p);
                break;
            case 2:
                p = ingresarApellido(p);
                break;
            case 3:
                p = ingresarEdad(p);
                break;
            case 4:
                break;
            default:
                printf("Operacion invalida, vuelve a intentar \n");
        }
    }


    return p;
}

void mostrarDatosPersona(Persona p){
    printf("Datos de la Persona \n");
    printf("Nombre: %s \n",p.nombre);
    printf("Apellido: %s \n",p.apellido);
    printf("Edad: %d \n",p.edad);
}



